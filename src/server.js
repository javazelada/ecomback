import express from 'express';
import routes from './routes/index.js';

const app = express();

app.use(express.json());
app.use('/api', routes);

const PORT = 3000;

app.listen(PORT, (error) => {
    if (error) {
        console.error('Error al iniciar el servidor:', error);
        process.exit(1); // Sale del proceso con un código de error
    } else {
        console.log(`Server running on http://localhost:${PORT}`);
    }
});

export default app; // Exportación con 'default'
