import express from "express";
import {check, validationResult} from "express-validator";
import sendResponse from "../utils/codeStatus.js";
import db from "../models/index.js";
import verifyJWTWithRole from "../middlewares/verifyJWTRole.js";
import multer from 'multer';
import uploadImageToImageServer from '../utils/uploadImage.js';

const router = express.Router();

// Configuración de almacenamiento de Multer
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    }
});

const upload = multer({storage: storage});

const allowedRoles = [1013];

router.get('/', async (req, res) => {
    try {
        const categories = await db.Categoria.findAll();
        return sendResponse(res, 200, categories);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

router.get('/maxUpdatedAt', async (req, res) => { // Ruta para sincronizar la base de datos
        try {
            const maxUpdatedAt = await db.Categoria.max('updatedAt');
            return sendResponse(res, 200, {maxUpdatedAt});
        } catch (error) {
            console.error(error);
            return sendResponse(res, 500);
        }
    }
);

router.post('/', upload.array('image'), verifyJWTWithRole(allowedRoles), [check('name', 'El nombre de la categoria es requerido.').not().isEmpty().isLength({min: 3}),
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return sendResponse(res, 400, {}, errors.array()[0].msg)
    }
    const {name, description, idCategory} = req.body;
    try {
        const categoryExist = await db.Categoria.findOne({where: {name: name}});

        if (categoryExist) {
            return sendResponse(res, 400, {}, 'Categoria ya registrada.');
        }
        const imageResponse = await uploadImageToImageServer(req.files);
        const imageObjects = imageResponse.imageUrl.map(image => ({
            url: image.path,
        }));
        console.log(imageObjects[0].url);

        const categoryCreate = await db.Categoria.create({
            name: name,
            description: description,
            urlImagen: imageObjects[0].url,
            idCategory: idCategory
        });
        return sendResponse(res, 201, categoryCreate, 'Categoria creada correctamente.');

    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

export default router;
