import express from "express";
import db from "../models/index.js";
import sendResponse from "../utils/codeStatus.js";
import jwt from "jsonwebtoken";
import verifyJWTWithRole from "../middlewares/verifyJWTRole.js";
import generateToken from "../utils/generateToken.js";
import decodeToken from "../utils/decodeToken.js";

const router = express.Router()
const allowedRoles = [1013, 1014, 1015];

router.get('/', verifyJWTWithRole(allowedRoles), async (req, res) => {
    try {
        const token = req.headers['authorization'].split(' ')[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const idUser = decoded.userId;
        if (!idUser) {
            return sendResponse(res, 400, {}, 'Datos invalidos.');
        }

        const directions = await db.MetodoPago.findAll({where: {idUser: idUser}});
        return sendResponse(res, 200, directions);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

router.post('/', verifyJWTWithRole(allowedRoles), async (req, res) => {
    try {
        const token = req.headers['authorization'].split(' ')[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const idUser = decoded.userId;
        if (!idUser) {
            return sendResponse(res, 400, {}, 'Datos invalidos.');
        }

        const {idType, tokenPayment} = req.body;
        const newPaymentMethod = await db.MetodoPago.create({idType: idType, token: tokenPayment, idUser: idUser});
        return sendResponse(res, 201, newPaymentMethod);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

router.post('/trj', verifyJWTWithRole(allowedRoles), async (req, res) => {
    try {
        const token = req.headers['authorization'].split(' ')[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const idUser = decoded.userId;
        if (!idUser) {
            return sendResponse(res, 400, {}, 'Datos invalidos.');
        }

        const {idType, fullName, cardNumber, cvv, expDate, isDefault} = req.body;
        if (isDefault) {
            await db.MetodoPago.update({isDefault: false}, {where: {idUser: idUser}});
        }
        const tokenPayment = generateToken(fullName, cardNumber, cvv, expDate);
        const newPaymentMethod = await db.MetodoPago.create({
            idType: idType,
            token: tokenPayment,
            isDefault: isDefault,
            idUser: idUser
        });
        return sendResponse(res, 201, newPaymentMethod);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

router.get('/trj', verifyJWTWithRole(allowedRoles), async (req, res) => {
    try {
        const token = req.headers['authorization'].split(' ')[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const idUser = decoded.userId;
        if (!idUser) {
            return sendResponse(res, 400, {}, 'Datos invalidos.');
        }

        const paymentMethods = await db.MetodoPago.findAll({
            where: {idUser: idUser}, include: [
                {
                    model: db.EstadoGeneral,
                    attributes: ['name'],
                    as: 'estadoGeneral'
                }
            ],
            order: [['createdAt', 'ASC']] // Change 'createdAt' to the desired attribute and 'ASC' to 'DESC' for descending order

        });

        const paymentDetails = paymentMethods.map(method => {
            const {fullName, cardNumber, cvv, expDate} = decodeToken(method.token);
            return {
                id: method.id,
                description: method.estadoGeneral.name,
                fullName,
                cardNumber: `**** **** **** ${cardNumber.slice(-4)}`,
                cvv: "***",
                expDate,
                isDefault: method.isDefault
            };
        });

        return sendResponse(res, 200, paymentDetails);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

router.put('/trj/:id', verifyJWTWithRole(allowedRoles), async (req, res) => {
    try {
        const token = req.headers['authorization'].split(' ')[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const idUser = decoded.userId;
        if (!idUser) {
            return sendResponse(res, 400, {}, 'Datos invalidos.');
        }

        const {id} = req.params;
        const {isDefault} = req.body;
        if (isDefault) {
            await db.MetodoPago.update({isDefault: false}, {where: {idUser: idUser}});
        }
        await db.MetodoPago.update({
            isDefault: isDefault
        }, {where: {id: id, idUser: idUser}});
        return sendResponse(res, 200);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

router.post('/trj/:id/def', verifyJWTWithRole(allowedRoles), async (req, res) => {
    try {
        const token = req.headers['authorization'].split(' ')[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const idUser = decoded.userId;
        if (!idUser) {
            return sendResponse(res, 400, {}, 'Datos invalidos.');
        }

        const {id} = req.params;
        await db.MetodoPago.update({isDefault: true}, {where: {id: id, idUser: idUser}});
        return sendResponse(res, 200);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

router.delete('/trj/:id', verifyJWTWithRole(allowedRoles), async (req, res) => {
    try {
        const token = req.headers['authorization'].split(' ')[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const idUser = decoded.userId;
        if (!idUser) {
            return sendResponse(res, 400, {}, 'Datos invalidos.');
        }

        const {id} = req.params;
        await db.MetodoPago.destroy({where: {id: id, idUser: idUser}});
        return sendResponse(res, 200);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});


export default router;
