import express from 'express';
import authRoutes from './authRoutes.js';
import categoryRoutes from "./categoryRoutes.js";
import productRoutes from "./productRoutes.js";
import shoppingCartRoutes from "./shoppingCartRoutes.js";
import paymentRoutes from "./paymentRoutes.js";
import addressRoutes from "./addressRoutes.js";

const router = express.Router();

router.use('/auth', authRoutes);
router.use('/category', categoryRoutes);
router.use('/product', productRoutes);
router.use('/cart', shoppingCartRoutes);
router.use('/address', addressRoutes);
router.use('/payment', paymentRoutes);

export default router;