import express from "express";
import db from "../models/index.js";
import sendResponse from "../utils/codeStatus.js";
import jwt from "jsonwebtoken";
import verifyJWTWithRole from "../middlewares/verifyJWTRole.js";

const router = express.Router()
const allowedRoles = [1013, 1014, 1015];

router.get('/', verifyJWTWithRole(allowedRoles), async (req, res) => {
    try {
        const token = req.headers['authorization'].split(' ')[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const idUser = decoded.userId;
        if (!idUser) {
            return sendResponse(res, 400, {}, 'Datos invalidos.');
        }

        const directions = await db.Direccion.findAll({
            where: {idUser: idUser}, attributes: {exclude: ['idUser']}
        });
        return sendResponse(res, 200, directions);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

router.post('/', verifyJWTWithRole(allowedRoles), async (req, res) => {
    try {
        const token = req.headers['authorization'].split(' ')[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const idUser = decoded.userId;
        if (!idUser) {
            return sendResponse(res, 400, {}, 'Datos invalidos.');
        }

        const {fullName, street, city, state, zipCode, country, phone, isMain} = req.body;

        // Si la nueva dirección es la principal, actualiza todas las demás direcciones del usuario a no principales
        if (isMain) {
            await db.Direccion.update({isMain: false}, {where: {idUser: idUser}});
        }

        const newDirection = await db.Direccion.create({
            fullName: fullName,
            street: street,
            city: city,
            state: state,
            zipCode: zipCode,
            country: country,
            phone: phone,
            isMain: isMain,
            idUser: idUser
        });
        return sendResponse(res, 201, newDirection, 'Dirección creada cor:rectamente.');
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});
export default router;

