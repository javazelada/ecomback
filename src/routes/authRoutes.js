import express from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import db from '../models/index.js';
import {check, validationResult} from 'express-validator';
import rateLimit from 'express-rate-limit';
import sendResponse from "../utils/codeStatus.js";


const router = express.Router();

const loginLimiter = rateLimit({
    windowMs: 15 * 60 * 1000,
    max: 50,
    message: "Demasiados intentos de inicio de sesión desde esta IP. Inténtalo de nuevo más tarde."
});

router.post('/signup', [
    // Validaciones básicas usando express-validator
    check('name', 'Nombre es requerido.').not().isEmpty(),
    check('email', 'Correo no válido.').isEmail(),
    check('password', 'La contraseña es requerida.').not().isEmpty().isLength({min: 6}),
    check('rol', 'El rol es requerido.').not().isEmpty()
], async (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        // return res.status(400).json({ status: 'error', message: errors.array()[0].msg });
        return sendResponse(res, 400, errors.array()[0].msg)
    }

    const t = await db.sequelize.transaction();

    try {
        const {name: userName, email: userEmail, password, rol} = req.body;

        // verificar si el nombre existe
        const nameExists = await db.Usuario.findOne({where: {name: userName}});
        if (nameExists) {
            return sendResponse(res, 400, 'Nombre ya registrado.')
        }
        // Verificar si el email ya existe
        const userExists = await db.Usuario.findOne({where: {email: userEmail}});
        if (userExists) {
            return sendResponse(res, 400, 'Correo electronico ya registrado.')
        }

        // Hashing del password
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(password, salt);

        const usuario = await db.Usuario.create({
            name: userName,
            email: userEmail,
            password: hashedPassword
        }, {transaction: t});

        await usuario.addEstadoGenerals(rol, {transaction: t});

        await t.commit();

        const { id, name, email } = usuario; // Destructuración para obtener solo los campos deseados
        // Generate JWT token
        const token = jwt.sign({ userId: usuario.id, roles: [rol] }, process.env.JWT_SECRET, { expiresIn: '1h' });
        const userData = { id, name, email }; // Crear el objeto con los campos seleccionados
        return sendResponse(res, 200, {userData, token}, 'Usuario creado correctamente.')

    } catch (error) {
        await t.rollback();
        console.error(error);
        return sendResponse(res, 500)
    }
});

router.post('/login', loginLimiter, async (req, res) => {
    try {
        const {emailUser, password} = req.body;
        const usuario = await db.Usuario.findOne({
            where: {email: emailUser},
            include: [{model: db.EstadoGeneral, as: 'EstadoGenerals', attributes: ['id']}]
        });

        if (!usuario) {
            return sendResponse(res, 404, {},'Usuario no encontrado.')
        }

        const isPasswordValid = bcrypt.compareSync(password, usuario.password);

        if (!isPasswordValid) {
            return sendResponse(res, 401, {},'Contraseña incorrecta.')
        }

        const roles = usuario.EstadoGenerals.map(role => role.id);
        const { id, name, email } = usuario; // Destructuración para obtener solo los campos deseados
        const token = jwt.sign({ userId: id, roles }, process.env.JWT_SECRET, { expiresIn: '1h' });
        const userData = { id, name, email }; // Crear el objeto con los campos seleccionados
        return sendResponse(res, 200, { token, userData })
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

export default router;
