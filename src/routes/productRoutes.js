import express from "express";
import db from "../models/index.js";
import sendResponse from "../utils/codeStatus.js";
import {check, validationResult} from "express-validator";
import verifyJWTWithRole from "../middlewares/verifyJWTRole.js";
import multer from 'multer';
import uploadImageToImageServer from '../utils/uploadImage.js';
import Resena from "../models/resena.js";
import {Op, Sequelize} from "sequelize";
import jwt from "jsonwebtoken";

const router = express.Router();

const allowedRoles = [1013, 1014, 1015];

// Configuración de almacenamiento de Multer
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    }
});

const upload = multer({storage: storage});

router.get('/', async (req, res) => {
    try {
        const allProducts = await db.Producto.findAll({
            include: [{
                model: db.ImagenProducto,
                attributes: ['url'],
                as: 'images'
            }]
        });
        return sendResponse(res, 200, allProducts);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

const productoEnCarrito = async (idUsuario, idProducto) => {
    const idEstadoCarritoActivo = 1016;

    const carrito = await db.Carrito.findOne({
        where: {
            idUser: idUsuario,
            idState: idEstadoCarritoActivo
        },
        include: {
            model: db.CarritoDetalle,
            as: 'cartDetails',
            where: {
                idProduct: idProducto
            }
        }
    });

    return carrito !== null; // Retorna true si el producto está en el carrito, false si no
};

router.get('/:id', verifyJWTWithRole(allowedRoles), async (req, res) => {
    const token = req.headers['authorization'].split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const idUser = decoded.userId;

    try {
        const id = req.params.id;
        const product = await db.Producto.findOne({
            where: {id: id},
            include: [
                {
                    model: db.Categoria,
                    attributes: ['name'],
                    as: 'Categorium',
                },
                {
                    model: db.ImagenProducto,
                    attributes: ['url'],
                    as: 'images'
                }
            ]
        });


        if (!product) {
            return sendResponse(res, 404, {}, 'Producto no encontrado.');
        }
        const estaEnCarrito = await productoEnCarrito(idUser, product.id);


        const response = {
            id: product.id,
            name: product.name,
            description: product.description,
            actualPrice: product.actualPrice,
            stock: product.stock,
            discount: product.discount,
            score: "0",
            totalReview: "0",
            category: product.Categorium.name,
            images: product.images.map(img => img.url), // suponiendo que ImagenProducto tiene una propiedad 'url'
            inCart: estaEnCarrito
        }


        return sendResponse(res, 200, response);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

router.get('/recentProducts/:idCategoria/:maxUpdatedAt?', verifyJWTWithRole(allowedRoles), async (req, res) => {
    const token = req.headers['authorization'].split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const idUser = decoded.userId;
    try {
        const { idCategoria, maxUpdatedAt } = req.params;
        let whereCondition = { idCategory: idCategoria };

        if (maxUpdatedAt) {
            whereCondition.updatedAt = { [Op.gt]: new Date(maxUpdatedAt) };
        }


        const products = await db.Producto.findAll({
            where: whereCondition,
            include: [
                {
                    model: db.Categoria,
                    attributes: ['name'],
                    as: 'Categorium',
                },
                {
                    model: db.ImagenProducto,
                    attributes: ['url'],
                    as: 'images'
                }
            ]
        });

        const flatProducts = products.map(async (product) => {
            const averageObj = await Resena.findOne({
                attributes: [
                    [Sequelize.fn('ROUND', Sequelize.fn('AVG', Sequelize.col('score'))), 'average'],
                    [Sequelize.fn('COUNT', Sequelize.col('*')), 'total']
                ],
                where: {idProduct: product.id},
                raw: true,
            });
            let {average, total} = averageObj;
            if (average == null)
                average = "0";
            const estaEnCarrito = await productoEnCarrito(idUser, product.id);

            return {
                id: product.id,
                name: product.name,
                description: product.description,
                actualPrice: product.actualPrice,
                stock: product.stock,
                discount: product.discount,
                score: average,
                totalReview: total,
                idCategory: product.idCategory,
                category: product.Categorium.name,
                images: product.images.map(img => img.url), // suponiendo que ImagenProducto tiene una propiedad 'url'
                inCart: estaEnCarrito
            };
        });

        const recentProducts = await Promise.all(flatProducts); // Esperar todos los productos

        return sendResponse(res, 200, recentProducts);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

router.get('/byCategory/:idCategoria', verifyJWTWithRole(allowedRoles), async (req, res) => {

    const token = req.headers['authorization'].split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const idUser = decoded.userId;

    try {
        const idCategoria = req.params.idCategoria;
        const averageObj = await Resena.findOne({
            attributes: [
                [Sequelize.fn('ROUND', Sequelize.fn('AVG', Sequelize.col('score'))), 'average'],
                [Sequelize.fn('COUNT', Sequelize.col('*')), 'total']
            ],
            raw: true,
        });
        let {average, total} = averageObj;
        if (average == null)
            average = "0";
        const products = await db.Producto.findAll({
            include: [
                {
                    model: db.Categoria,
                    attributes: ['name'],
                    where: {id: idCategoria},
                    as: 'Categorium',
                },
                {
                    model: db.ImagenProducto,
                    attributes: ['url'],
                    as: 'images'
                }
            ],
        });
        const flatProducts = products.map(async (product) => {
            const estaEnCarrito = await productoEnCarrito(idUser, product.id);

            return {
                id: product.id,
                name: product.name,
                description: product.description,
                actualPrice: product.actualPrice,
                stock: product.stock,
                discount: product.discount,
                score: average,
                totalReview: total,
                category: product.Categorium.name,
                images: product.images.map(img => img.url), // suponiendo que ImagenProducto tiene una propiedad 'url'
                inCart: estaEnCarrito
            };
        });

        const productsWithFlag = await Promise.all(flatProducts); // Esperar todos los productos

        return sendResponse(res, 200, productsWithFlag);
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});


router.post('/', upload.array('image'), verifyJWTWithRole(allowedRoles), [
    check('name', 'El nombre del producto es requerido.').not().isEmpty(),
    check('description', 'La descripcion del producto es requerido.').not().isEmpty(),
    check('actualPrice', 'El precio actual es requerido.').not().isEmpty(),
    check('stock', 'El stock del producto es requerido.').not().isEmpty(),
    check('idCategory', 'la categoria es requerida.').not().isEmpty()
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return sendResponse(res, 400, {}, errors.array()[0].msg);
    }

    const {name, description, actualPrice, stock, discount, idCategory} = req.body;

    try {
        const categoryExist = await db.Categoria.findOne({where: {id: idCategory}});

        if (!categoryExist) {
            return sendResponse(res, 400, {}, 'La categoria no existe.')
        }

        const productExist = await db.Producto.findOne({where: {name: name}});

        if (productExist) {
            return sendResponse(res, 400, {}, 'Producto ya registrado.');
        }

        const productCreate = await db.Producto.create({
            name: name,
            description: description,
            actualPrice: actualPrice,
            stock: stock,
            discount: discount,
            idCategory: idCategory
        });

        if (req.files) {
            const imageResponse = await uploadImageToImageServer(req.files);
            const imageObjects = imageResponse.imageUrl.map(image => ({
                url: image.path,
                idProduct: productCreate.id
            }));
            console.log(imageObjects);

            if (imageObjects && imageObjects.length > 0) {
                let response = await db.ImagenProducto.bulkCreate(imageObjects);
                console.log('Imágenes insertadas correctamente:', response);
            }

        }

        return sendResponse(res, 201, productCreate, 'Producto creado correctamente.');
    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

export default router;
