import express from "express";

import jwt from 'jsonwebtoken';
import sendResponse from "../utils/codeStatus.js";
import db from "../models/index.js";
import verifyJWTWithRole from "../middlewares/verifyJWTRole.js";

const router = express.Router();

const allowedRoles = [1013, 1014, 1015];
const outstanding = 1016;

router.get('/', verifyJWTWithRole(allowedRoles), async (req, res) => {

    try {
        const token = req.headers['authorization'].split(' ')[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const idUser = decoded.userId;
        if (!idUser) {
            return sendResponse(res, 400, {}, 'Datos invalidos.');
        }

        const cart = await db.Carrito.findOne({
            where: {idUser: idUser},
            include: [{
                model: db.CarritoDetalle,
                include: [db.Producto]
            }]
        });
        return sendResponse(res, 200, cart);


    } catch (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});
router.post('/addProduct', verifyJWTWithRole(allowedRoles), async (req, res) => {
    const {idProduct, quantity} = req.body;
    const token = req.headers['authorization'].split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const idUser = decoded.userId;
    let cart;
    let product

    try {
        const searchCart = await db.Carrito.findOne({
            where: {
                idUser: idUser,
                idState: outstanding
            }
        });

        if (searchCart) {
            cart = searchCart;
        } else {
            // crear carrito
            cart = await db.Carrito.create({idUser: idUser, idState: outstanding});
        }
    } catch (e) {
        return sendResponse(res, 400, {}, 'cart creation Datos invalidos.');
    }

    try {
        product = await db.Producto.findOne({
            where: {
                id: idProduct
            }
        });
        if (!product) {
            return sendResponse(res, 400, {}, 'find product Datos invalidos.');
        }
    } catch (e) {
        return sendResponse(res, 500, {}, "error 1");
    }


    try {
        // Calcular el descuento por item
        const descuentoItem = (product.discount / 100) * product.actualPrice;
        const precioConDescuento = product.actualPrice - descuentoItem;
        console.log("montos --->", descuentoItem, precioConDescuento);

        await db.CarritoDetalle.create({
            quantity: quantity,
            idCart: cart.id,
            idProduct: product.id,
            unitPrice: product.actualPrice,
            priceDiscount: precioConDescuento,
            discountedAmount: descuentoItem,
            discount: product.discount
        });
    } catch (e) {
        return sendResponse(res, 500, {}, "error 2");
    }

    try {
        const detailsCart = await db.CarritoDetalle.findAll({
            where: {
                idCart: cart.id
            }
        });

        // const detailsCart = await Promise.all(detailsCartPromises);

        console.log('detallesCarrito --->', detailsCart);
        console.log('carrito --->', cart)

        // Calcular subtotal
        const subTotal = detailsCart.reduce((acc, detalle) => acc + (detalle.quantity * detalle.unitPrice), 0);

        // Calcular total (aplicando el descuento)
        const total = detailsCart.reduce((acc, detalle) => acc + (detalle.quantity * detalle.priceDiscount), 0);

        // Total items
        const totalItems = detailsCart.reduce((acc, detalle) => acc + (1 * detalle.quantity), 0);

        const totalDescuento = detailsCart.reduce((acc, detalle) => acc + (detalle.quantity * detalle.discountedAmount), 0);

        // Actualizar el total del carrito

        console.log('montos ---> ', subTotal, total, totalItems, totalDescuento)

        const carrito = await db.Carrito.findByPk(cart.id);

        if (!carrito) {
            return res.status(404).send('Carrito no encontrado');
        }

        carrito.set({
            totalItems: totalItems,
            subTotal: subTotal,
            totalDiscount: totalDescuento,
            total: total
        });
        await carrito.save();

        // await cart.update({total, subTotal, totalItems, totalDescuento, outstanding}); // Incluir subTotal

        return sendResponse(res, 201, {}, 'Carrito creado correctamente');
    } catch (e) {
        return sendResponse(res, 500, {}, "error 3");
    }
});

router.get('/detail', verifyJWTWithRole(allowedRoles), async (req, res) => {
    const token = req.headers['authorization'].split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const idUser = decoded.userId;
    let cart;

    try {
        cart = await db.Carrito.findOne({
            where: {
                idUser: idUser,
                idState: outstanding
            },
            include: [{
                model: db.CarritoDetalle,
                as: 'cartDetails',
                include: [{
                    model: db.Producto,
                    as: 'product',
                    include: [{
                        model: db.ImagenProducto,
                        as: 'images',
                        attributes: {
                            exclude: ['createdAt', 'updatedAt']
                        },
                    }],
                    attributes: {
                        exclude: ['createdAt', 'updatedAt']
                    },
                }],
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                },
            }]
        });
        if (!cart) {
            return res.status(404).send('Carrito no encontrado');
        }
        return sendResponse(res, 200, cart);

    } catch (e) {
        return sendResponse(res, 500);
    }

});

router.post('/', verifyJWTWithRole(allowedRoles), async (req, res) => {
    const token = req.headers['authorization'].split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const idUser = decoded.userId;

    try {
        const {products} = req.body;
        // validacion de entrada
        if (!idUser || !products || products.length === 0) {
            return sendResponse(res, 400, {}, 'Datos invalidos.');
        }

        // const searchCart = await db.Carrito.findOne({
        //     where: {
        //         idUsuario: idUser,
        //         idEstado: outstanding
        //     }
        // });
        // let cart;
        //
        // if (searchCart) {
        //     cart = searchCart;
        // } else {
        //     // crear carrito
        //     cart = await db.Carrito.create({idUsuario: idUser, idEstado: outstanding});
        // }

        const cart = await db.Carrito.create({idUser: idUser, idState: outstanding});


        // crear detalles carrito
        const detallesCarrito = await Promise.all(
            products.map(async (product) => {
                // const producto = await db.Producto.findOne( product.id);
                const producto = await db.Producto.findOne({
                    where: {
                        id: product.id
                    }
                });

                if (!producto) {
                    return sendResponse(res, 400, {}, 'Datos invalidos.');
                }

                // Calcular el descuento por item
                const descuentoItem = (producto.discount / 100) * producto.actualPrice;
                const precioConDescuento = producto.actualPrice - descuentoItem;

                return await db.CarritoDetalle.create({
                    quantity: product.quantity,
                    idCart: cart.id,
                    idProduct: product.id,
                    unitPrice: producto.unitPrice,
                    priceDiscount: precioConDescuento,
                    discountedAmount: descuentoItem,
                    discount: producto.discount
                });

            }));


        const detailsCart = await db.CarritoDetalle.findAll({
            where: {
                idCart: cart.id
            }
        });

        // const detailsCart = await Promise.all(detailsCartPromises);

        console.log('detallesCarrito --->', detailsCart);
        console.log('carrito --->', cart)

        // Calcular subtotal
        const subTotal = detallesCarrito.reduce((acc, detalle) => acc + (detalle.quantity * detalle.unitPrice), 0);

        // Calcular total (aplicando el descuento)
        const total = detallesCarrito.reduce((acc, detalle) => acc + (detalle.quantity * detalle.priceDiscount), 0);

        // Total items
        const totalItems = detallesCarrito.reduce((acc, detalle) => acc + (1 * detalle.quantity), 0);

        const totalDescuento = detallesCarrito.reduce((acc, detalle) => acc + (detalle.quantity * detalle.discountedAmount), 0);

        // Actualizar el total del carrito

        console.log('montos ---> ', subTotal, total, totalItems, totalDescuento)

        const carrito = await db.Carrito.findByPk(cart.id);

        if (!carrito) {
            return res.status(404).send('Carrito no encontrado');
        }

        carrito.set({
            totalItems: totalItems,
            subTotal: subTotal,
            totalDiscount: totalDescuento,
            total: total
        });
        await carrito.save();

        // await cart.update({total, subTotal, totalItems, totalDescuento, outstanding}); // Incluir subTotal

        return sendResponse(res, 201, carrito, 'Carrito creado correctamente');

    } catch
        (error) {
        console.error(error);
        return sendResponse(res, 500);
    }
});

export default router