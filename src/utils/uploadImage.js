import axios from 'axios';
import FormData from 'form-data';
import fs from 'fs';

async function uploadImageToImageServer(files) {
    const formData = new FormData();
    files.forEach((file) => {
        formData.append('image', fs.createReadStream(file.path));
    });

    try {
        const response = await axios.post('http://localhost:3001/upload', formData, {
            headers: {
                ...formData.getHeaders()
            }
        });
        if (!response.data.state) {
            throw new Error(response.data.message);
        }

        console.log('imagenes a eliminar', response.data);
        // Elimina el archivo temporal una vez que la imagen se ha subido con éxito
        deleteFiles(files);
        console.log('respuesta del servidor de imagenes', response.data);
        return response.data; // Esto incluirá el mensaje y la URL de la imagen
    } catch (error) {
        console.error('Error al subir la imagen:', error.response?.data || error.message);
        throw error; // Reenviar o manejar el error según sea necesario
    }
}

async function deleteFiles(files) {
    for (const file of files) {
        console.log('iteracion ', file);
        await fs.promises.unlink(file.path);
        console.log(`Archivo temporal eliminado: ${file.path}`);
    }
}

export default uploadImageToImageServer;
