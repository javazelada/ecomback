import crypto from 'crypto';

function decodeToken(token) {
    const [ivHex, encrypted] = token.split(':');
    const key = crypto.createHash('sha256').update(process.env.TOKEN_SECRET).digest();
    const iv = Buffer.from(ivHex, 'hex');
    const decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
    let decrypted = decipher.update(encrypted, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    const [fullName, cardNumber, cvv, expDate] = decrypted.split('|');
    return {fullName, cardNumber, cvv, expDate};
}

export default decodeToken;