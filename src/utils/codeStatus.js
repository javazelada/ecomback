
function sendResponse(res, statusCode, data = {}, message = '') {
    let response = {
        success: true,
        data: data
    };

    // Configurar el estado y el mensaje basándose en el código de estado HTTP
    switch (statusCode) {
        case 200: // 200 OK: La solicitud fue exitosa.
            message = message || 'OK';
            break;
        case 201: // 201 Created: La solicitud fue exitosa y se creó un recurso.
            message = message || 'Created';
            break;
        case 204: // 204 No Content: La petición se ha completado con éxito, pero su respuesta no tiene ningún contenido.
            message = message || 'No Content';
            response.data = null; // No se envía contenido con un 204
            break;
        case 400: // 400 Bad Request: La solicitud no puede ser procesada debido a errores de sintaxis.
            response.success = false;
            message = message || 'Bad Request';
            break;
        case 401: // 401 Unauthorized: El cliente debe autenticarse para obtener la respuesta solicitada.
            response.success = false;
            message = message || 'Unauthorized';
            break;
        case 403: // 403 Forbidden: El cliente no tiene derechos para acceder al contenido.
            response.success = false;
            message = message || 'Forbidden';
            break;
        case 404: // 404 Not Found: El servidor no encuentra el recurso solicitado.
            response.success = false;
            message = message || 'Not Found';
            break;
        case 500: // 500 Internal Server Error: El servidor ha encontrado una situación que no sabe cómo manejar.
            response.success = false;
            message = message || 'Internal Server Error';
            break;
        default:
            response.success = false;
            message = message || 'An unexpected error occurred';
    }

    if ((statusCode === 200 || statusCode === 201) && data) {
        if (Array.isArray(data)) {
            data.forEach(item => {
                if (item && item.dataValues && typeof item.dataValues === 'object') {
                    delete item.dataValues.createdAt;
                    delete item.dataValues.updatedAt;
                }
            });
        } else if (data.dataValues && typeof data.dataValues === 'object') {
            delete data.dataValues.createdAt;
            delete data.dataValues.updatedAt;
        }
    }

    response.message = message;
    res.status(statusCode).json(response);
}

export default sendResponse;
