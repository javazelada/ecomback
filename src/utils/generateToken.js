import crypto from 'crypto';

function generateToken(fullName, cardNumber, cvv, expDate) {
    const tokenSecret = process.env.TOKEN_SECRET;
    if (!tokenSecret) {
        throw new Error('TOKEN_SECRET environment variable is not defined');
    }

    const key = crypto.createHash('sha256').update(tokenSecret).digest();
    const iv = crypto.randomBytes(16); // Initialization vector.
    const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
    let encrypted = cipher.update(`${fullName}|${cardNumber}|${cvv}|${expDate}`, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return `${iv.toString('hex')}:${encrypted}`;
}

export default generateToken;