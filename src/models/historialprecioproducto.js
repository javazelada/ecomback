import { Model } from 'sequelize';

export default class HistorialPrecioProducto extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                price: DataTypes.DOUBLE,
                startDate: DataTypes.STRING,
                endDate: DataTypes.STRING,
                idProduct: DataTypes.INTEGER
            },
            {
                sequelize,
                modelName: 'HistorialPrecioProducto',
            }
        );
    }

    static associate(models) {
        HistorialPrecioProducto.belongsTo(models.Producto, {foreignKey: 'idProduct'})
    }
}
