import { Model } from 'sequelize';

export default class Transaccion extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                amount: DataTypes.DOUBLE,
                date: DataTypes.STRING,
                idState: DataTypes.INTEGER,
                idPaymentMethod: DataTypes.INTEGER
            }, {
                sequelize,
                modelName: 'Transaccion',
            }
        );
    }

    static associate(models) {
        Transaccion.belongsTo(models.EstadoGeneral, {foreignKey: 'idState'});
        Transaccion.hasMany(models.Carrito, {foreignKey: 'idTransaction'});
        Transaccion.belongsTo(models.MetodoPago, { foreignKey: 'idPaymentMethod' });
    }
}
