import { Model } from 'sequelize';

export default class Usuario extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                name: DataTypes.STRING,
                email: DataTypes.STRING,
                password: DataTypes.STRING,
            }, {
            sequelize,
            modelName: 'Usuario',
        }
        );
    }

    static associate(models) {
        Usuario.belongsToMany(models.EstadoGeneral, {
            through: models.UsuarioRoles,
            foreignKey: 'idUser',
            otherKey: 'idState'
        }); 
        Usuario.hasMany(models.Direccion, { foreignKey: 'idUser' })
        Usuario.hasMany(models.Carrito, { foreignKey: 'idUser' })
        Usuario.hasMany(models.MetodoPago, { foreignKey: 'idUser' })
        Usuario.hasMany(models.Resena, { foreignKey: 'idUser' })
    }
}
