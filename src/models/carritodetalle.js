import {Model} from 'sequelize';

export default class CarritoDetalle extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                quantity: DataTypes.INTEGER,
                unitPrice: DataTypes.DECIMAL(10, 2),
                discount: DataTypes.INTEGER,
                priceDiscount: DataTypes.DECIMAL(10, 2),
                discountedAmount: DataTypes.DECIMAL(10, 2),
                idCart: DataTypes.INTEGER,
                idProduct: DataTypes.INTEGER
            },
            {
                sequelize,
                modelName: 'CarritoDetalle',
            }
        );
    }

    static associate(models) {
        CarritoDetalle.belongsTo(models.Carrito, {foreignKey: 'idCart'})
        CarritoDetalle.belongsTo(models.Producto, {foreignKey: 'idProduct', as : 'product'})
    }
}