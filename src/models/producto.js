import {Model} from 'sequelize';

export default class Producto extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                name: DataTypes.STRING,
                description: DataTypes.TEXT,
                actualPrice: DataTypes.DECIMAL(10, 2),
                discount: DataTypes.INTEGER,
                stock: DataTypes.INTEGER,
                idCategory: DataTypes.INTEGER
            },
            {
                sequelize,
                modelName: 'Producto',
            }
        );
    }

    static associate(models) {
        Producto.belongsTo(models.Categoria, {foreignKey: 'idCategory'})
        Producto.hasMany(models.CarritoDetalle, {foreignKey: 'idProduct'})
        Producto.hasMany(models.Resena, {foreignKey: 'idProduct'})
        Producto.hasMany(models.HistorialPrecioProducto, {foreignKey: 'idProduct'})
        Producto.hasMany(models.ImagenProducto, {foreignKey: 'idProduct', as: 'images'})
    }
}
