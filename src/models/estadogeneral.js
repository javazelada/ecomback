import {Model} from 'sequelize';

export default class EstadoGeneral extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                name: DataTypes.STRING,
                description: DataTypes.STRING,
                type: DataTypes.STRING
            },
            {
                sequelize,
                modelName: 'EstadoGeneral',
            }
        );
    }

    static associate(models) {
        EstadoGeneral.hasMany(models.Transaccion, {foreignKey: 'idEstate'});
        EstadoGeneral.hasMany(models.MetodoPago, {foreignKey: 'idType'});
        EstadoGeneral.belongsToMany(models.Usuario, {
            through: models.UsuarioRoles,
            foreignKey: 'idState',
            otherKey: 'idUser'
        });
    }
}



