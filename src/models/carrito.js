import { Model } from 'sequelize';

export default class Carrito extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                totalItems: DataTypes.INTEGER,
                subTotal: DataTypes.DECIMAL(10,2),
                taxes: DataTypes.DECIMAL(10,2),
                totalDiscount: DataTypes.DECIMAL(10,2),
                total: DataTypes.DECIMAL(10,2),
                idUser: DataTypes.INTEGER,
                idState: DataTypes.INTEGER,
                idTransaction: DataTypes.INTEGER,
                // idMetodoPago: DataTypes.INTEGER
            },
            {
                sequelize,
                modelName: 'Carrito'
            },
        );
    }

    static associate(models) {
        Carrito.hasMany(models.CarritoDetalle, { foreignKey: 'idCart', as: 'cartDetails'});
        Carrito.belongsTo(models.Usuario, { foreignKey: 'idUser' });
        Carrito.belongsTo(models.EstadoGeneral, { foreignKey: 'idState' });
        Carrito.belongsTo(models.Transaccion, { foreignKey: 'idTransaction' });
        // Carrito.belongsTo(models.MetodoPago, { foreignKey: 'idMetodoPago' });

    }
}
