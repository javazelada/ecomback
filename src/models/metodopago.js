import {Model} from 'sequelize';

export default class MetodoPago extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                idType: DataTypes.INTEGER,
                token: DataTypes.STRING,
                idUser: DataTypes.INTEGER,
                isDefault: DataTypes.BOOLEAN
            },
            {
                sequelize,
                modelName: 'MetodoPago',
            }
        );
    }

    static associate(models) {
        MetodoPago.belongsTo(models.EstadoGeneral, {foreignKey: 'idType', as: 'estadoGeneral'});
        MetodoPago.hasMany(models.Transaccion, {foreignKey: 'idPaymentMethod'});
        MetodoPago.belongsTo(models.Usuario, {foreignKey: 'idUser'});
    }
}
