import { Model } from 'sequelize';

export default class Resena extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                comment: DataTypes.STRING,
                score: DataTypes.INTEGER,
                idProduct: DataTypes.INTEGER,
                idUser: DataTypes.INTEGER
            }, {
                sequelize,
                modelName: 'Resena',
            }
        );
    }

    static associate(models) {
        Resena.belongsTo(models.Producto, {foreignKey: 'idProduct'})
        Resena.belongsTo(models.Usuario, {foreignKey: 'idUser'})
    }
}
