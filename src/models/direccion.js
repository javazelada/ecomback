import { Model } from 'sequelize';

export default class Direccion extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                fullName: DataTypes.STRING,
                street: DataTypes.STRING,
                city: DataTypes.STRING,
                state: DataTypes.STRING,
                zipCode: DataTypes.STRING,
                phone: DataTypes.STRING,
                country: DataTypes.STRING,
                isMain: DataTypes.BOOLEAN,
                idUser: DataTypes.INTEGER
            },
            {
                sequelize,
                modelName: 'Direccion',
            }
        );
    }

    static associate(models) {
        Direccion.belongsTo(models.Usuario, {foreignKey: 'idUser'})
    }
}
