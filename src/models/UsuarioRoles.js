import { Model } from 'sequelize';

export default class UsuarioRoles extends Model {
    static init(sequelize, DataTypes) {
        return super.init({}, {
            sequelize,
            modelName: 'UsuarioRoles',
        });
    }

    static associate(models) {
        // No es necesario establecer relaciones aquí; se hará en los otros modelos.
    }
}
