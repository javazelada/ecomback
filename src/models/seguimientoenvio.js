import {Model} from 'sequelize';

export default class SeguimientoEnvio extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                date: DataTypes.STRING,
                currentLocation: DataTypes.STRING,
                idState: DataTypes.INTEGER,
                idShipment: DataTypes.INTEGER
            }, {
                sequelize,
                modelName: 'SeguimientoEnvio',
            }
        );
    }

    static associate(models) {
    }
}
