import { Model } from 'sequelize';

export default class EstadoTransaccion extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                name: DataTypes.STRING,
                description: DataTypes.STRING
            },
            {
                sequelize,
                modelName: 'EstadoTransaccion',
            }
        );
    }

    static associate(models) {
        EstadoTransaccion.hasMany(models.Transaccion, {foreignKey: 'idState'})
    }
}
