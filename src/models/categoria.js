import {Model} from 'sequelize';

export default class Categoria extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                name: DataTypes.STRING,
                description: DataTypes.TEXT,
                urlImagen: DataTypes.STRING,
                idCategory: DataTypes.INTEGER
            },
            {
                sequelize,
                modelName: 'Categoria',
            }
        );
    }

    static associate(models) {
        Categoria.belongsTo(models.Categoria, {foreignKey: 'idCategory'})
        Categoria.hasMany(models.Categoria, {foreignKey: 'idCategory'})
        Categoria.hasMany(models.Producto, {foreignKey: 'idCategory'})
    }
}