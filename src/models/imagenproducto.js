import {Model} from 'sequelize';

export default class ImagenProducto extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                url: DataTypes.STRING,
                idProduct: DataTypes.INTEGER
            },
            {
                sequelize,
                modelName: 'ImagenProducto',
            }
        );
    }

    static associate(models) {
        ImagenProducto.belongsTo(models.Producto, {foreignKey: 'idProduct'});
    }
}
