import { Model } from 'sequelize';

export default class DatosEnvio extends Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                estimatedDate: DataTypes.STRING,
                actualDate: DataTypes.STRING,
                idState: DataTypes.INTEGER,
                idTransaction: DataTypes.INTEGER,
                idAddress: DataTypes.INTEGER
            },
            {
                sequelize,
                modelName: 'DatosEnvio',
            }
        );
    }

    static associate(models) {

    }
}