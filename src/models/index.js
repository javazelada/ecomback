import { Sequelize } from 'sequelize';
import configData from '../config/config.js';

import Carrito from './carrito.js';
import CarritoDetalle from "./carritodetalle.js";
import Categoria from "./categoria.js";
import DatosEnvio from "./datosenvio.js";
import Direccion from "./direccion.js";
import EstadoGeneral from "./estadogeneral.js";
import HistorialPrecioProducto from "./historialprecioproducto.js";
import ImagenProducto from "./imagenproducto.js";
import MetodoPago from "./metodopago.js";
import Producto from "./producto.js";
import Resena from "./resena.js";
import SeguimientoEnvio from "./seguimientoenvio.js";
import Transaccion from "./transaccion.js";
import Usuario from "./usuario.js";
import UsuarioRoles from "./UsuarioRoles.js";


const env = process.env.NODE_ENV || 'development';
const config = configData[env];
const db = {};

const sequelize = config.use_env_variable
    ? new Sequelize(process.env[config.use_env_variable], config)
    : new Sequelize(config.database, config.username, config.password, config);

sequelize.authenticate()
    .then(() => {
        console.log('Connection to the database has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });


process.on('exit', closeDatabase).on('SIGINT', closeDatabase).on('SIGTERM', closeDatabase);

function closeDatabase() {
    console.log("Cerrando las conexiones a la base de datos...");
    sequelize.close();
    process.exit(0);  // sal del proceso
}


const models = [
    EstadoGeneral,
    Usuario,
    Categoria,
    Direccion,
    Producto,
    Carrito,
    CarritoDetalle,
    Resena,
    HistorialPrecioProducto,
    ImagenProducto,
    MetodoPago,
    Transaccion,
    DatosEnvio,
    SeguimientoEnvio,
    UsuarioRoles
];

// Inicializar todos los modelos
models.forEach(model => model.init(sequelize, Sequelize.DataTypes));

// Agregar los modelos al objeto db
models.forEach(model => {
    db[model.name] = model;
});

// Configurar las asociaciones para cada modelo
models.forEach(model => {
    if (model.associate) {
        model.associate(db);
    }
});

db.sequelize = sequelize;

export default db;
