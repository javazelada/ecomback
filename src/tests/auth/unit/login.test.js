import chai from 'chai';
import sinon from 'sinon';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken'; // Agregar esta línea para importar jsonwebtoken
import db from'../../../models/index.js'; // Ruta a tu modelo db
import sendResponse from'../../../utils/codeStatus.js';
import authRouter from'../../../routes/authRoutes.js'; // Ruta a tu controlador de autenticación
import app from '../../../server.js'; // Ruta a tu aplicación Express inicializada
import supertest from 'supertest'; // Importar 'supertest'
import sinonChai from 'sinon-chai'; // Importar sinon-chai

chai.use(sinonChai); // Integrar sinon-chai con Chai
const { expect } = chai;

describe('Auth Router Tests: Login', () => {
    let sandbox;
    const request = supertest(app); // Crear una instancia de 'supertest' para tu app

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it('should return 404 if user is not found', async () => {
        const req = { body: { email: 'noexiste@example.com', password: 'password' } };
        const res = {
            status: sinon.stub().returnsThis(),
            json: sinon.stub() // O sinon.spy()
        };

        // Stub de findOne para que devuelva null
        sandbox.stub(db.Usuario, 'findOne').resolves(null);

        // Petición a la ruta de login utilizando 'supertest'
        const response = await request.post('/api/auth/login').send(req.body);

        expect(response.status).to.equal(404); // Verificando el código de estado
        // expect(sendResponse).to.have.been.calledWith(res, 404, 'Usuario no encontrado.'); // Aserción original
    });

    it('should return 401 if password is incorrect', async () => {
        const req = { body: { email: 'example@example.com', password: 'incorrectpassword' } };
        const res = {
            status: sinon.stub().returnsThis(),
            json: sinon.stub()
        };

        sandbox.stub(db.Usuario, 'findOne').resolves({ /* Mock usuario encontrado */ });
        sandbox.stub(bcrypt, 'compareSync').returns(false); // Simular contraseña incorrecta

        const response = await request.post('/api/auth/login').send(req.body);

        expect(response.status).to.equal(401);
    });

    it('should return 200 with token and user data on successful login', async () => {
        const userData = {
            id: 1001,
            name: 'Administrador',
            email: 'root@admin.com',
            password: '$2b$10$ufqF136.atJNNwqUvMcnW.VKEHDkzq2rTzwshrtEsfozyRR89DeOi',
            createdAt: new Date('2024-04-04T02:59:55.946Z'),
            updatedAt: new Date('2024-04-04T02:59:55.946Z'),
            EstadoGenerals: [{ id: 1013, UsuarioRoles: [{ /* Detalles del rol */ }] }]
        };

        sandbox.stub(db.Usuario, 'findOne').resolves(userData);
        sandbox.stub(bcrypt, 'compareSync').returns(true);
        sandbox.stub(jwt, 'sign').returns('mocked.jwt.token');

        const response = await request.post('/api/auth/login')
            .send({ email: 'roote@admin.com', password: 'root' });

        expect(response.status).to.equal(200);
        expect(response.body).to.have.property('data');
        expect(response.body.data).to.have.property('token').to.be.a('string');
        expect(response.body.data.token).to.equal('mocked.jwt.token'); // Aquí deberías comparar el token devuelto

    });



});
