import {expect} from 'chai';
import supertest from 'supertest';
import db from '../../../models/index.js'; // Ruta a tu modelo db
import jwt from 'jsonwebtoken';
import app from '../../../server.js';
import bcrypt from "bcrypt";

describe('Auth Router Tests: Login Integration', () => {
    beforeEach(async () => {
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash('password123', salt);
        // // Crear un usuario de prueba en la base de datos
        await db.Usuario.create({
            name: 'John Doe',
            email: 'johndoe@example.com',
            password: hashedPassword,
            rol: 1014
        });
    });

    afterEach(async () => {
        await db.Usuario.destroy({where: {email: 'johndoe@example.com'}});
    });

    it('should login successfully and return a JWT token', async () => {
        const req = {email: 'johndoe@example.com', password: 'password123'};
        const [res] = await Promise.all([supertest(app)
            .post('/api/auth/login')
            .send(req)
            .expect(200)]);

        // Verificar la estructura de la respuesta
        expect(res.body.data).to.have.property('token');
        expect(res.body.data).to.have.property('usuarioData');

        // Decodificar y verificar el token JWT
        const tokenData = jwt.decode(res.body.data.token, process.env.JWT_SECRET);
        expect(tokenData).to.have.property('idUser');
        expect(tokenData).to.have.property('roles');

        // Verificar la información del usuario
        const usuarioData = res.body.data.usuarioData;
        expect(usuarioData).to.have.property('id');
        expect(usuarioData).to.have.property('name');
        expect(usuarioData).to.have.property('email');
        expect(usuarioData).to.not.have.property('password'); // La contraseña no debe incluirse en la respuesta
    });
});
