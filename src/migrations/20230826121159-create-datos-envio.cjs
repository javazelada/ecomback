'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('DatosEnvios', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      estimatedDate: {
        type: Sequelize.STRING
      },
      actualDate: {
        type: Sequelize.STRING
      },
      idState: {
        type: Sequelize.INTEGER,
        references: {
          model: 'EstadoGenerals',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      idTransaction: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Transaccions',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      idAddress: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Direccions',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('DatosEnvios');
  }
};