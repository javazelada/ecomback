'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        await queryInterface.bulkDelete('EstadoGenerals', null, {});


        await queryInterface.bulkInsert('EstadoGenerals', [
            {
                id: 1001,
                name: 'En Espera',
                description: 'Envío aún no ha sido procesado',
                type: 'DatosEnvio',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1002,
                name: 'En Ruta',
                description: 'El paquete está en camino',
                type: 'DatosEnvio',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1003,
                name: 'Entregado',
                description: 'El paquete ha sido entregado al cliente',
                type: 'DatosEnvio',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1004,
                name: 'Retrasado',
                description: 'El paquete está retrasado',
                type: 'DatosEnvio',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1005,
                name: 'Recibido',
                description: 'El paquete ha sido recibido por la empresa de envío',
                type: 'SeguimientoEnvio',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1006,
                name: 'En Tránsito',
                description: 'El paquete se encuentra en tránsito',
                type: 'SeguimientoEnvio',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1007,
                name: 'Problema Detectado',
                description: 'Se ha detectado un problema con el envío',
                type: 'SeguimientoEnvio',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1008,
                name: 'Cercano a Entregar',
                description: 'El paquete está cerca de ser entregado',
                type: 'SeguimientoEnvio',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1009,
                name: 'Pendiente',
                description: 'Transacción no completada',
                type: 'Transaccion',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1010,
                name: 'En Proceso',
                description: 'Transacción en proceso',
                type: 'Transaccion',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1011,
                name: 'Completada',
                description: 'Transacción completada con éxito',
                type: 'Transaccion',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1012,
                name: 'Cancelada',
                description: 'Transacción cancelada',
                type: 'Transaccion',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1013,
                name: 'Administrador',
                description: 'Usuario con todos los permisos',
                type: 'Rol',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1014,
                name: 'Usuario',
                description: 'Usuario con permisos básicos',
                type: 'Rol',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1015,
                name: 'Invitado',
                description: 'Usuario sin permisos de edición',
                type: 'Rol',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1016,
                name: 'Pendiente',
                description: 'El carrito ha sido creado pero aún no se ha confirmado el pedido. El cliente puede elegir pagar ahora o contra entrega.',
                type: 'Carrito',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1017,
                name: 'Confirmado',
                description: 'El pedido ha sido realizado y el pago ha sido procesado (si el cliente no seleccionó "Pagar al momento de entregar").',
                type: 'Carrito',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1018,
                name: 'En proceso',
                description: 'El pedido está siendo ensamblado o preparado para su envío.',
                type: 'Carrito',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1019,
                name: 'En camino',
                description: 'El pedido está en camino hacia el cliente.',
                type: 'Carrito',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1020,
                name: 'Pago contra entrega pendiente',
                description: 'El pedido ha sido enviado, pero el pago aún no se ha realizado.',
                type: 'Carrito',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1021,
                name: 'Entregado',
                description: 'El cliente ha recibido el pedido y el pago ha sido completado (si el cliente seleccionó "Pagar al momento de entregar").',
                type: 'Carrito',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1022,
                name: 'Cancelado',
                description: 'El pedido ha sido cancelado por el cliente o por el sistema.',
                type: 'Carrito',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1023,
                name: 'Tarjeta de Crédito',
                description: 'Pago con tarjeta de crédito',
                type: 'MetodoPago',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1024,
                name: 'Tarjeta de Débito',
                description: 'Pago con tarjeta de débito',
                type: 'MetodoPago',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1025,
                name: 'PayPal',
                description: 'Pago con PayPal',
                type: 'MetodoPago',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 1026,
                name: 'QR',
                description: 'Pago con código QR',
                type: 'MetodoPago',
                createdAt: new Date(),
                updatedAt: new Date()
            },
        ], {});
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete('EstadoGenerals', null, {});
    }
};
