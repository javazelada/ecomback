import dotenv from 'dotenv';
dotenv.config();

const configData = {
  development: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    host: process.env.DB_HOST,
    dialect: 'postgres',
    logging: (msg) => {
      console.log(new Date(), msg); // Esto agregará una marca de tiempo a cada log
    },  },

  production: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    host: process.env.DB_HOST,
    dialect: 'postgres',
    // ... config
  },
  // ... other envs
};

export default configData;
