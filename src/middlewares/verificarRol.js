import { db } from '../models';

const checkRole = role => async (req, res, next) => {
    const user = await db.Usuario.findByPk(req.userId);
    if (user && user.role !== role) {
        return res.status(403).send({ message: 'Acceso denegado' });
    }
    next();
};

export default checkRole;