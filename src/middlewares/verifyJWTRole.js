import jwt from 'jsonwebtoken';

// Función middleware para verificar JWT y rol
const verifyJWTWithRole = (allowedRoles) => (req, res, next) => {
    const authHeader = req.headers['authorization'];

    if (!authHeader) {
        return res.status(401).send('Acceso denegado: no se proporcionó token.');
    }

    const tokenParts = authHeader.split(' ');
    if (tokenParts.length !== 2 || tokenParts[0] !== 'Bearer') {
        return res.status(401).send('Acceso denegado: formato de token inválido.');
    }

    const token = tokenParts[1];

    try {
        const verified = jwt.verify(token, process.env.JWT_SECRET);

        // Verificar si el usuario tiene el rol permitido
        if (verified.roles.some(role =>
            allowedRoles.includes(role)
        )){
            req.user = verified;
            next(); // Pasar al siguiente middleware o ruta
        } else {
            res.status(403).send('Acceso denegado: rol no permitido.');
        }

    } catch (err) {
        res.status(400).send('Token inválido.');
    }
};

export default verifyJWTWithRole;
